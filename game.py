import pygame
from ball import Ball


class Game:

    def __init__(self):
        self.width = 1600
        self.height = 900
        self.win = pygame.display.set_mode((self.width, self.height))
        self.players_width = 20
        self.players_height = 150
        self.run = True
        self.clicked = False

        # player attributes
        self.player_x = 50
        self.player_y = self.height // 2
        self.player_score = 0

        # opponent attributes
        self.opponent_x = self.width - 50
        self.opponent_y = self.height // 2
        self.opponent_score = 0

    def draw(self, ball, font):
        # fill the window with black
        self.win.fill((0, 0, 0))
        # draw player
        pygame.draw.rect(self.win, (255, 255, 255), (self.player_x - self.players_width / 2, self.player_y - self.players_height / 2, self.players_width, self.players_height))

        # draw opponent
        pygame.draw.rect(self.win, (255, 255, 255), (self.opponent_x - self.players_width / 2, self.opponent_y - self.players_height / 2, self.players_width, self.players_height))

        # draw scores
        text_player_score = font.render(str(self.player_score), 1, (255, 255, 255))
        self.win.blit(text_player_score, (self.width // 2 - text_player_score.get_width() - 25, 10))
        text_opponent_score = font.render(str(self.opponent_score), 1, (255, 255, 255))
        self.win.blit(text_opponent_score, (self.width // 2 + 25, 10))

        # draw ball
        ball.draw(self.win)

        pygame.display.update()

    def ball_vertical_collide_with_players(self, ball, players_x, players_y):
        return (players_x - self.players_width // 2 <= ball.x <= players_x + self.players_width // 2) \
                and ((ball.y + ball.width // 2 >= players_y - self.players_height // 2 and ball.y - ball.width // 2 < players_y + self.players_height // 2)
                or (ball.y - ball.width // 2 <= players_y + self.players_height // 2 and ball.y - ball.width // 2 > players_y - self.players_height // 2))

    def ball_collide(self, ball):
        # checking collisions with side of players
        if ((self.player_y - self.players_height // 2 <= ball.y <= self.player_y + self.players_height // 2) and (self.player_x - self.players_width < ball.x - ball.width // 2 <= self.player_x + self.players_width // 2)) \
                or ((self.opponent_y - self.players_height // 2 <= ball.y <= self.opponent_y + self.players_height // 2) and (self.opponent_x + self.players_width > ball.x + ball.width // 2 >= self.opponent_x - self.players_width // 2)):
            ball.vx = -ball.vx
            ball.increase_speed(1)

        # collision with the wall or above or below the player
        elif not (0 < ball.y < self.height) \
                or self.ball_vertical_collide_with_players(ball, self.player_x, self.player_y) or self.ball_vertical_collide_with_players(ball, self.opponent_x, self.opponent_y):
            ball.vy = -ball.vy

    def is_ball_out(self, ball):
        if ball.x < 0:
            self.opponent_score += 1
            ball.reset(self)
        elif ball.x > self.width:
            self.player_score += 1
            ball.reset(self)

    def play(self):
        pygame.init()
        font = pygame.font.Font("retro.ttf", 70)
        pygame.display.set_caption("Pong")
        clock = pygame.time.Clock()
        ball = Ball(self.width // 2, self.height // 2, 4, 3)
        ball.reset(self)

        # main game loop
        while self.run:
            clock.tick(60)

            # player movement
            (mouse_x, mouse_y) = pygame.mouse.get_pos()
            if mouse_y < self.players_height // 2:
                self.player_y = self.players_height // 2
            elif mouse_y > self.height - self.players_height // 2:
                self.player_y = self.height - self.players_height // 2
            else:
                self.player_y = mouse_y

            # opponent movement (TODO atm perfect)
            if ball.y < self.players_height // 2:
                self.opponent_y = self.players_height // 2
            elif ball.y > self.height - self.players_height // 2:
                self.opponent_y = self.height - self.players_height // 2
            else:
                self.opponent_y = ball.y

            if self.clicked:
                ball.x += ball.vx
                ball.y += ball.vy
                self.is_ball_out(ball)
                self.ball_collide(ball)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.clicked = True

            self.draw(ball, font)

        pygame.quit()


# main
game = Game()
game.play()




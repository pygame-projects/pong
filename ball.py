import pygame
import random


class Ball:

    def __init__(self, x, y, vx, vy):
        self.x = x
        self.y = y
        self.width = 10
        self.vx = vx
        self.vy = vy

    def draw(self, win):
        pygame.draw.circle(win, (255, 255, 255), (self.x, self.y), self.width)

    def reset(self, game):
        self.x = game.width // 2
        self.y = game.height // 2
        pygame.event.clear()
        game.clicked = False
        # we don't want vx = 0 and we don't want it to be too slow
        self.vx = random.choice([-4, -3, 3, 4])
        self.vy = random.randrange(-3, 4)

    def increase_speed(self, increase):
        self.vx = self.vx + increase if self.vx >= 0 else self.vx - increase
        self.vy = self.vy + increase if self.vy >= 0 else self.vy - increase
